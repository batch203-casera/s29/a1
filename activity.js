db.users.find(
    {
        $or: [
            {firstName: /s/i},
            {lastName: /d/i}
        ]
},
{
    firstName: 1,
    lastName: 1,
    _id: 0
}
);

db.users.find(
    {
        $and: [
            {department: "HR"},
            {age: {$gte: 70}}
        ]
    }
);

db.users.find(
    {
        $and: [
            {firstName: /e/i},
            {age: {$lte: 30}}
        ]
    }
);